<?php
include_once("_config.php");
include_once("classes/medicament.class.php");

switch($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        $response = Medicament::get();
        break;
    case "PUT":
        $response = Medicament::insert($data);
        break;
    case "POST":
        $response = Medicament::update($data);
        break;
}

if ($response != null && $response != "")
    echo json_encode($response);