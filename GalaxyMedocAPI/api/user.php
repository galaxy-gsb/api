<?php
include_once("_config.php");
include_once("classes/user.class.php");

$service = $_GET["service"];

switch($service) {
    case "connexion":
        $response = User::get($data);
        break;
    case "recover":
        $response = User::recover($data);
        break;
}

if ($response != null && $response != "")
    echo json_encode($response);