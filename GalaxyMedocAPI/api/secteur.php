<?php
include_once("_config.php");
include_once("classes/secteur.class.php");

switch($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        $response = Secteur::get();
        break;
    case "PUT":
        $response = Secteur::insert($data);
        break;
    case "POST":
        $response = Secteur::delete($data);
        break;
}

if ($response != null && $response != "")
    echo json_encode($response);