<?php
class CommandeDetails
{
    public static function get($idCommande) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_CommandeDetails_Select(?)");
        $request->bindParam(1, $idCommande);
        $request->execute();
        $commandeDetails = $request->fetchAll(PDO::FETCH_OBJ);

        foreach ($commandeDetails as $commandeDetail) {
            $commandeDetail->Medicament = new stdClass();
            $commandeDetail->Medicament->Id = $commandeDetail->MedicamentId;
            $commandeDetail->Medicament->Nom = $commandeDetail->MedicamentNom;
            $commandeDetail->Medicament->Composition = $commandeDetail->MedicamentComposition;
            $commandeDetail->Medicament->Effets = $commandeDetail->MedicamentEffets;
            $commandeDetail->Medicament->ContreIndications = $commandeDetail->MedicamentContreIndications;
            $commandeDetail->Medicament->Prix = $commandeDetail->MedicamentPrix;
            $commandeDetail->Medicament->Categorie = new stdClass();
            $commandeDetail->Medicament->Categorie->Id = $commandeDetail->MedicamentCategorieId;
            $commandeDetail->Medicament->Categorie->Nom = $commandeDetail->MedicamentCategorieNom;
            $commandeDetail->Medicament->Categorie->TauxRemboursement = $commandeDetail->MedicamentCategorieRemboursement;

            $commandeDetail->MontantRembourse = $commandeDetail->Medicament->Prix * $commandeDetail->Medicament->Categorie->TauxRemboursement;
            $commandeDetail->PrixTotal = floatval($commandeDetail->Medicament->Prix) * intval($commandeDetail->Quantite);

            unset($commandeDetail->MedicamentId);
            unset($commandeDetail->MedicamentNom);
            unset($commandeDetail->MedicamentComposition);
            unset($commandeDetail->MedicamentEffets);
            unset($commandeDetail->MedicamentContreIndications);
            unset($commandeDetail->MedicamentPrix);
        }

        return $commandeDetails;
    }

    public static function insert($commande, $commandeDetails) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_CommandeDetails_Insert(?, ?, ?)");
        $request->bindParam(1, $commande->Id);
        $request->bindParam(2, $commandeDetails->Medicament->Id);
        $request->bindParam(3, $commandeDetails->Quantite);
        $request->execute();
        $result = $request->fetch();
        $commandeDetails->Id = $result[0];
        return $commandeDetails->Id;
    }

    public static function delete($commandeDetails) {
        $db = new PDO(CONNECTION_STRING, CONNECTION_USER, CONNECTION_PASSWORD);
        $request = $db->prepare("CALL sp_CommandeDetails_Delete(?)");
        $request->bindParam(1, $commandeDetails->CommandeId);
        $request->execute();
        $rows = $request->rowCount();
        return $rows;
    }
}