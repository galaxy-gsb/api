﻿/*===================================================================================*/
/*	ANGULARJS : CONTROLLER - MENU
/*===================================================================================*/

app.controller("MenuCtrl", function (UsersSvc, $location) {
    var self = this;
    self.user = UsersSvc;

    self.deconnexion = function () {
        self.user = null;
        UsersSvc.deconnexion();
    }

    self.getActive = function (path) {
        return ($location.path().substr(0, path.length) === path) ? "active" : "";
    }
});